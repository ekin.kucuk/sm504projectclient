import React,{useState} from "react";
import "./LoginPage.css";
import { useNavigate } from 'react-router-dom';
import axios  from "axios";
import { useDispatch } from "react-redux";
import { setToken } from "../../redux/authSlice";
const LoginPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
const dispatch = useDispatch()
  const navigate = useNavigate()
  const toSignup = ()=>{
    navigate("/signup")
  }
  const onEmailChange = (e) => {
    const mail = e.target.value;
    setEmail(mail);
  };

  const onPasswordChange = (e) => {
    const passwd = e.target.value;
    setPassword(passwd);
  };

  const onLoginPressed = (e)=>{
    e.preventDefault()
    if(email !== "" && password !== ""){
      let payload={}
      payload.email= email;
      payload.password = password;
      axios.post("http://localhost:8080/auth/login",payload).then(res=>{
        const {token} = res.data
        localStorage.setItem("jwtToken", token);
        dispatch(setToken(token))
        navigate("/")
      })
    }else{
      alert("Email or password cannot be empty")
    }

  }
  return (
    <div className="auth-wrapper">
      <div className="auth-inner">
        <form>
          <h3>Sign In</h3>
          <div className="form-group">
            <label>Email address</label>
            <input
              type="email"
              className="form-control"
              placeholder="Enter email"
              value={email}
              onChange={onEmailChange}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control"
              placeholder="Enter password"
              value={password}
              onChange={onPasswordChange}
            />
          </div>
        <br/>
          <button  type="button" className="btn btn-primary btn-block" onClick={onLoginPressed}>
            Sign in
          </button>
          <p className="forgot-password text-right">
            Don't have an account <span className="highLighter" onClick={toSignup}>sign up?</span>
          </p>
        </form>
      </div>
    </div>
  );
};

export default LoginPage;
