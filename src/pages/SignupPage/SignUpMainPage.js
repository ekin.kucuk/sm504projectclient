import React from "react";
import { Button, Container, Row, Col } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "./SingupManinpage.css";
const SignUpMainPage = () => {
    const navigate = useNavigate();
    const onCompanySignup = ()=>{
        navigate("/signupcomp")
    }
    const onSeekerSignup = ()=>{
        navigate("/signuppers")
    }
  return (
    <Container id="smp-container">
      <Row>
        <Col>
          <h3>NewCareer</h3>
        </Col>
      </Row>
      <br />
      <Row>
        <Col>
          <Button onClick={onCompanySignup} >Register As Company</Button>
        </Col>
        <Col>
          <Button onClick={onSeekerSignup}>Register As Job Seeker</Button>
        </Col>
      </Row>
    </Container>
  );
};

export default SignUpMainPage;
