import React, { useState } from "react";
import { FormControl, InputGroup, Form, Button } from "react-bootstrap";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import "./Signupperson.css";
const SignupPerson = () => {
  const inputArr = [
    {
      type: "text",
      id: 1,
      value: "",
      year: "",
    },
  ];
  const expArr = [
    {
      type: "text",
      id: 1,
      value: "",
      duration: "",
    },
  ];

  const [education, setEducation] = useState(inputArr);
  const [experience, setExperience] = useState(expArr);
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [currentlyWork, setCurrentlyWork] = useState(false);
  const [currentPos, setCurrentPos] = useState("");
  const [currentComp, setCurrentComp] = useState("");
  const navigate = useNavigate();
  const onNameChange = (e) => {
    const val = e.target.value;
    setName(val);
  };
  const onSurnameChange = (e) => {
    const val = e.target.value;
    setSurname(val);
  };
  const onEmailChange = (e) => {
    const val = e.target.value;
    setEmail(val);
  };
  const onPasswdChange = (e) => {
    const val = e.target.value;
    setPassword(val);
  };

  const onEducationChange = (e, i) => {
    const val = e.target.value;
    let copy = [...education];
    let copyItem = { ...copy[i] };

    copyItem.value = val;
    copy[i] = copyItem;
    setEducation(copy);
  };
  const onGradYearChange = (e, i) => {
    const val = e.target.value;
    let copy = [...education];
    let copyItem = { ...copy[i] };
    copyItem.year = val;
    copy[i] = copyItem;
    setEducation(copy);
  };
  const onExperienceChange = (e, i) => {
    const val = e.target.value;
    let copy = [...experience];
    let copyItem = { ...copy[i] };
    copyItem.value = val;
    copy[i] = copyItem;
    setExperience(copy);
  };
  const onExpDurationChange = (e, i) => {
    const val = e.target.value;
    let copy = [...experience];
    let copyItem = { ...copy[i] };
    copyItem.duration = val;
    copy[i] = copyItem;
    setExperience(copy);
  };
  const addEducation = () => {
    setEducation((s) => {
      return [
        ...s,
        {
          type: "text",
          value: "",
        },
      ];
    });
  };
  const addExperience = () => {
    setExperience((s) => {
      return [
        ...s,
        {
          type: "text",
          value: "",
        },
      ];
    });
  };
  const signUp = () => {
    let signUpObject = {};
    if (
      name === "" ||
      surname === "" ||
      email === "" ||
      password === "" ||
      education.length < 1
    ) {
      alert("Please fill all fields");
    } else {
      signUpObject.name = name;
      signUpObject.surname = surname;
      signUpObject.email = email;
      signUpObject.password = password;
      if (currentlyWork === true) {
        if (currentPos === "" || currentComp === "") {
          alert("Please fill all fields");
        } else {
          signUpObject.jobTitle = currentPos + " at " + currentComp;
        }
      }
      if (experience.length > 0) {
        let expStr = "";

        experience.forEach((data) => {
          expStr += data.value + " " + data.duration + ", ";
        });
        signUpObject.pastExperience = expStr;
      }
      let edStr = "";
      education.forEach((item) => {
        edStr += item.value + " " + item.year + ", ";
      });
      signUpObject.education = edStr;
      axios.post("http://localhost:8080/auth/user/signup",signUpObject).then(res=>{
        if(res.data){
          if (res.data) {
            alert("Successfully registered");
            window.location.href = "/login";
          }
        }
      })
    }
  };

  const onCurrentlyWork = (e) => {
    const val = e.target.checked;
    setCurrentlyWork(val);
  };
  const onCurrentPosChange = (e) => {
    const val = e.target.value;
    setCurrentPos(val);
  };
  const onCurrentCompChange = (e) => {
    const val = e.target.value;
    setCurrentComp(val);
  };
  const toLogin = () => {navigate("/login");};
  return (
    <div className="auth-wrapper">
      <div className="auth-inner">
        <Form id="persform">
          <h3>Job Seeker Sign Up</h3>
          <div className="form-group">
            <label>Name</label>
            <input
              type="text"
              className="form-control"
              placeholder="Name"
              value={name}
              onChange={onNameChange}
            />
          </div>
          <div className="form-group">
            <label>Surname</label>
            <input
              type="text"
              className="form-control"
              placeholder="Surname"
              value={surname}
              onChange={onSurnameChange}
            />
          </div>
          <div className="form-group">
            <label>Email address</label>
            <input
              type="email"
              className="form-control"
              placeholder="Enter email"
              value={email}
              onChange={onEmailChange}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control"
              placeholder="Enter password"
              onChange={onPasswdChange}
            />
            </div>
          <div className="form-group">
            <label>Education</label>
            {education.map((item, i) => {
              return (
                <div key={"ed" + i}>
                  <InputGroup>
                    <InputGroup.Text>{"Education " + (i + 1)}</InputGroup.Text>
                    <input
                      type={item.type}
                      size="40"
                      className="form-control"
                      onChange={(e) => onEducationChange(e, i)}
                    />
                  </InputGroup>

                  <InputGroup>
                    <InputGroup.Text>Gradution Year</InputGroup.Text>
                    <input
                      type="number"
                      size="30"
                      className="form-control"
                      onChange={(e) => onGradYearChange(e, i)}
                    />
                  </InputGroup>
                  <br />
                </div>
              );
            })}
            <Button onClick={addEducation}>Add</Button>
          </div>
          <div className="form-group">
            <label>Do you currently work?</label>
            <input
              type="checkbox"
              onChange={onCurrentlyWork}
              value={currentlyWork}
            />
          </div>
          {currentlyWork && (
            <div>
              <div className="form-group">
                <label>Title</label>
                <input
                  className="form-control"
                  type="text"
                  onChange={onCurrentPosChange}
                  value={currentPos}
                />
              </div>
              <div className="form-group">
                <label>Company</label>
                <input
                  className="form-control"
                  type="text"
                  onChange={onCurrentCompChange}
                  value={currentComp}
                />
              </div>
            </div>
          )}
          <div className="form-group">
            <label>Experience</label>
            {experience.map((item, i) => {
              return (
                <div key={"exp" + i}>
                  <InputGroup>
                    <InputGroup.Text>{"Experience " + (i + 1)}</InputGroup.Text>
                    <input
                      type={item.type}
                      size="40"
                      className="form-control"
                      onChange={(e) => onExperienceChange(e, i)}
                    />
                  </InputGroup>

                  <InputGroup>
                    <InputGroup.Text>Duration</InputGroup.Text>
                    <input
                      type="text"
                      size="30"
                      className="form-control"
                      onChange={(e) => onExpDurationChange(e, i)}
                    />
                  </InputGroup>
                  <br />
                </div>
              );
            })}
            <Button onClick={addExperience}>Add</Button>
          </div>
          <br />
          <Button className=" btn-block" onClick={signUp}>
            Sign Up
          </Button>

        
        </Form>
        <p className="forgot-password text-right">
            Already registered{" "}
            <span className="highLighter" onClick={toLogin}>
              sign in?
            </span>
          </p>
      </div>
    </div>
  );
};

export default SignupPerson;
