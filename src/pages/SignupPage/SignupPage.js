import React, { useState } from "react";
import "./SignupPage.css";
import {
  CountryDropdown,
  RegionDropdown,
  CountryRegionData,
} from "react-country-region-selector";
import { useNavigate } from "react-router-dom";
import { FormControl, InputGroup, Form, Button } from "react-bootstrap";
import axios from "axios";
const SignupPage = () => {
  const [country, setCountry] = useState("Turkey");
  const [city, setCity] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [about, setAbout] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const toLogin = () => {
    navigate("/login");
  };
  const selectCountry = (val) => {
    setCountry(val);
  };
  const selectCity = (val) => {
    setCity(val);
  };

  const onEmailChange = (e) => {
    const val = e.target.value;
    setEmail(val);
  };

  const onPasswdChange = (e) => {
    const val = e.target.value;
    setPassword(val);
  };

  const onAboutChange = (e) => {
    const val = e.target.value;
    setAbout(val);
  };

  const onCompanyNameChange = (e) => {
    const val = e.target.value;
    setCompanyName(val);
  };

  const signUp = () => {
    if (
      companyName === "" ||
      email === "" ||
      password === "" ||
      about === "" ||
      city === "" ||
      country === ""
    ) {
      alert("Please fill all fields");
    } else if (password.length < 8) {
      alert("Password must be at least 8 characters");
    } else {
      let signUpObject = {};
      signUpObject.companyName = companyName;
      signUpObject.email = email;
      signUpObject.password = password;
      signUpObject.about = about;
      signUpObject.city = city;
      signUpObject.country = country;
      axios
        .post("http://localhost:8080/auth/company/signup", signUpObject)
        .then((res) => {
          if (res.data) {
            alert("Successfully registered");
            window.location.href = "/login";
          }
        });
    }
  };
  return (
    <div className="auth-wrapper">
      <div className="auth-inner">
        <Form>
          <h3>Company Sign Up</h3>

          <div className="form-group">
            <label>Company name</label>
            <input
              type="text"
              className="form-control"
              placeholder="Company name"
              onChange={onCompanyNameChange}
            />
          </div>
          <Form.Label htmlFor="about">About</Form.Label>
          <InputGroup>
            <FormControl
              id="about"
              as="textarea"
              aria-label="With textarea"
              onChange={onAboutChange}
            />
          </InputGroup>
          <br />
          <div>
            <CountryDropdown
              value={country}
              onChange={(val) => selectCountry(val)}
            />
            <RegionDropdown
              country={country}
              value={city}
              onChange={(val) => selectCity(val)}
            />
          </div>
          <div className="form-group">
            <label>Email address</label>
            <input
              type="email"
              className="form-control"
              placeholder="Enter email"
              onChange={onEmailChange}
            />
          </div>

          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control"
              placeholder="Enter password"
              onChange={onPasswdChange}
            />
          </div>
          <br />
          <Button className=" btn-block" onClick={signUp}>
            Sign Up
          </Button>

          <p className="forgot-password text-right">
            Already registered{" "}
            <span className="highLighter" onClick={toLogin}>
              sign in?
            </span>
          </p>
        </Form>
      </div>
    </div>
  );
};

export default SignupPage;
