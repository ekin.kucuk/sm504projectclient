import React, { useState, useEffect } from "react";
import "./Home.css";
import Header from "../../components/Header/Header";
import LeftBar from "../../components/LeftBar/LeftBar";
import CompanyAbout from "../../components/CompanyAbout/CompanyAbout";
import CompanyJobs from "../../components/CompanyJobs/CompanyJobs";
import JobPostingCreator from "../../components/JobPostingCreator/JobPostingCreator";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useDispatch } from "react-redux";
import { setCompany } from "../../redux/companySlice";
import AllJobs from "../../components/AllJobs/AllJobs";
import UserApplication from "../../components/UserApplication/UserApplication";
const Home = ({ getHeaderInfo }) => {
  const [openAbout, setOpenAbout] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [openJobs, setOpenJobs] = useState(false);
  const [companyInfo, setCompanyInfo] = useState({});
  const [isCompanyUser, setIsCompanyUser] = useState(true);
  const [userId,setUserId]= useState()
  const dispatch = useDispatch();
  useEffect(() => {
    const jwtToken = localStorage.getItem("jwtToken");
    if (jwtToken) {
      const decoded = jwt_decode(jwtToken);
      const user_id = decoded.id;
      axios.defaults.headers.common["Authorization"] = jwtToken;
      getCompanyInfo(user_id);
    }
  }, []);
  const getCompanyInfo = (id) => {
    axios.get(`http://localhost:8080/company/byuser/${id}`).then((res) => {
      if (res.data) {
        dispatch(setCompany(res.data));
        setCompanyInfo(res.data);
      } else {
        setIsCompanyUser(false);
      }
    }).catch(err=>{
      setIsCompanyUser(false);
      setUserId(id)
    })
  };
  const onNavCliked = (key) => {
    if (key === "create") {
      setOpenCreate(true);
      setOpenAbout(false);
      setOpenJobs(false);
    } else if (key === "jobs") {
      setOpenCreate(false);
      setOpenAbout(false);
      setOpenJobs(true);
    } else {
      setOpenCreate(false);
      setOpenAbout(true);
      setOpenJobs(false);
    }
  };
  return (
    <div>
      <Header info={companyInfo} />
      <LeftBar onNavCliked={onNavCliked} isCompanyUser={isCompanyUser}/>
      {isCompanyUser && (
        <div id="home-content-wrapper">
          {openAbout && <CompanyAbout />}
          {openCreate && <JobPostingCreator />}
          {openJobs &&  <CompanyJobs />}
        </div>
      )}
      { !isCompanyUser && (
        <div id="home-content-wrapper">
        {openCreate && <UserApplication userId={userId} />}
         {openJobs && <AllJobs userId={userId} />}
         </div>
      )}
    </div>
  );
};

export default Home;
