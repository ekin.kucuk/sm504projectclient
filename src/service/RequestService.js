import axios from "axios";
const BASE_URL = "http://localhost:8080";

export const sendGetRequest = (url) => {
  axios.get(BASE_URL + url);
};

export const sendPostRequest = (url,payload) => {
    axios.post(BASE_URL + url,payload);
  };