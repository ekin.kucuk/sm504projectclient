import React, { useState } from "react";
import "./JobPostingCreator.css";
import { Form, Container, Row, Col, Button } from "react-bootstrap";
import { useSelector } from "react-redux";
import axios from "axios";
const JobPostingCreator = () => {
  const [jobTitle, setJobTitle] = useState("");
  const [description, setDescription] = useState("");
  const companyInfo =useSelector(state=>state.company.company)

  const onJobTitleChange = (e) => {
      const jt = e.target.value
      setJobTitle(jt)
  };
  const onDescriptionChange = (e)=>{
    const desc = e.target.value
    setDescription(desc)
  }
  const postJob = () => {
      
      if(jobTitle !== "" && description !==""){
        const companyId = companyInfo.id;
        let payload = {}
        payload.positionName = jobTitle;
        payload.description = description;
        payload.companyId = companyId
        axios.post("http://localhost:8080/job/create",payload).then(res=>{
          if(res.data){
            setJobTitle("")
            setDescription("")
            alert("New Job Posting created")
          }
        })
      }else{
        alert("Job Title and description cannot be blank")
      }
  };

  return (
    <div id="job-creator-wrapper">
    <Container>
      <Form>
        <Row>
          <Col>
            <h3>Create New Job Posting</h3>
          </Col>
        </Row>
        <Row>
        <Col>
        <Form.Label htmlFor="job-title">Job Title:</Form.Label>
        </Col>
          <Col lg="8" xl="8">
            <Form.Group className="mb-3">
              <Form.Control
                className="job-input"
                type="text"
                value={jobTitle}
                onChange={onJobTitleChange}
                placeholder="Enter Job Title"
              />
            </Form.Group>
          </Col>
        </Row>
        <Row>
        <Col>
        <Form.Label>Job Description:</Form.Label>
        </Col>
          <Col lg="8" xl="8">
            <Form.Group className="mb-3">
            
              <Form.Control
                className="job-input"
                type="textrea"
                as={"textarea"}
                value={description}
                onChange={onDescriptionChange}
                placeholder="Enter Job Description"
              />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <Button onClick={postJob}>Share Job</Button>
          </Col>
        </Row>
      </Form>
    </Container>
    </div>
  );
};

export default JobPostingCreator;
