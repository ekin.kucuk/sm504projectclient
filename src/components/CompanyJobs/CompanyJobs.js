import React, { useState, useEffect } from "react";
import JobPosting from "../JobPosting/JobPosting";
import { useSelector } from "react-redux";
import axios from "axios";
import { Container, Row, Col,Modal,Button} from "react-bootstrap";
import "./companyJobs.css";
import Applicants from "../Applicants/Applicants";
const CompanyJobs = () => {
  const [jobs, setJobs] = useState([]);
  const [detail,setDetail] = useState(false);
  const[selectedJob,setSelectedJob] = useState(null);
  const companyInfo = useSelector((state) => state.company.company);
  useEffect(() => {
    const companyId = companyInfo.id;
    getJobPostings(companyId);
  }, []);

  const getJobPostings = (companyId) => {
      axios.get(`http://localhost:8080/job/getByCompanyId/${companyId}`)
      .then((res) => {
        const result = res.data;
        if (result) {
          setJobs(res.data);
        }
      });
  };
  const reviewApplicants = (id) => {
    setSelectedJob(id)
    setDetail(true)
  };
  const handleClose =()=>{
    setDetail(false)
  }
  return (
    <Container fluid>
      <Row>
        {jobs.map((job, index) => (
          <Col xl={4} lg={4} key={index} className="myjob-col">
            <JobPosting
              companyName={job.company.companyName}
              jobTitle={job.positionName}
              description={job.description}
              isApplicable={false}
              review={() => reviewApplicants(job.id)}
            />
          </Col>
        ))}
      </Row>
      {detail && selectedJob != null &&
      <Modal show={true} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>List of Applicants</Modal.Title>
        </Modal.Header>
        <Modal.Body><Applicants jobId={selectedJob}/></Modal.Body>
      </Modal> }
    </Container>
  );
};

export default CompanyJobs;
