import React, { useState,useEffect } from "react";
import "./allJobs.css";
import JobPosting from "../JobPosting/JobPosting";
import axios from "axios";
import { Container, Row, Col } from "react-bootstrap";
const AllJobs = ({userId}) => {
  const [jobs, setJobs] = useState([]);
  useEffect(() => {
     
    getJobPostings();
  }, []);
  const getJobPostings = () => {
    axios.get("http://localhost:8080/job/getall/").then((res) => {
      const result = res.data;
     
      setJobs(result);
    });
  };

  const applyToAJob = (jobId) => {
      const payload = {
          userId: userId,
          jobId: jobId,
          date: ""
      }
axios.post("http://localhost:8080/jobapplication/create",payload).then(res=>{
    if(res.data){
        alert("Successfully applied");
    }
})

  };
  return (
    <Container fluid>
      <Row>
        {jobs.map((job, index) => (
          <Col xl={4} lg={4} key={index} className="myjob-col">
            <JobPosting
              companyName={job.company.companyName}
              jobTitle={job.positionName}
              description={job.description}
              isApplicable={true}
              apply={()=>applyToAJob(job.id)}
            />
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default AllJobs;
