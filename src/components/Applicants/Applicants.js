import axios from "axios";
import React, { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
const Applicants = ({ jobId }) => {
  const [applicantList, setApplicantList] = useState([]);

  useEffect(() => {
    getListofApplicants(jobId);
  }, [jobId]);
  const getListofApplicants = (jobId) => {
    axios
      .get(`http://localhost:8080/jobapplication/getapplicants/${jobId}`)
      .then((res) => {
        if (res.data) {
          const result = res.data;
        
          let unique = [
            ...new Map(result.map((item) => [item["id"], item])).values(),
        ];
          console.log(unique);
          setApplicantList(unique);
        }
      });
  };
  return (
    <Container fluid>
      {applicantList.map((applicant, index) => (
        <div key={index}>
          <Row >
            <Col>Name: {applicant.name}</Col>
            <Col>Surname: {applicant.surname}</Col>
            <Col>Job Title: {applicant.jobTitle}</Col>
          </Row>
          <Row>
          <Col>Email: {applicant.genericSysUser.email}</Col>
              <Col>Education: {applicant.education}</Col>
              <Col>Experience: {applicant.pastExperience}</Col>
          </Row>
          <hr />
        </div>
      ))}
    </Container>
  );
};

export default Applicants;
