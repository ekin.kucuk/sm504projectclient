import React, { useEffect, useState } from "react";
import { Col, Row, Nav } from "react-bootstrap";
import "./LeftBar.css";
const LeftBar = ({onNavCliked ,isCompanyUser}) => {
  useEffect(()=>{

  },[isCompanyUser])

  return (
    <div id="left-bar-container">
      <div id="left-bar-wrapper">
      {isCompanyUser && 
        <Nav variant="pills" className="flex-column">
        
          <Nav.Link eventKey="create" onClick={()=>{onNavCliked("create")}}> Create Job Posting</Nav.Link>
          <Nav.Link eventKey="jobs" onClick={()=>{onNavCliked("jobs")}}>My Jobs</Nav.Link>
          <Nav.Link eventKey="about" onClick={()=>{onNavCliked("about")}}>About</Nav.Link>
      
        </Nav>
      }
      {isCompanyUser === false && 
        <Nav variant="pills" className="flex-column">
        
          <Nav.Link eventKey="applications" onClick={()=>{onNavCliked("create")}}> My applications</Nav.Link>
          <Nav.Link eventKey="jobs" onClick={()=>{onNavCliked("jobs")}}>Jobs</Nav.Link>
          <Nav.Link eventKey="profile" onClick={()=>{onNavCliked("about")}}>Profile</Nav.Link>
      </Nav>
      }
      </div>
    </div>
  );
};

export default LeftBar;
