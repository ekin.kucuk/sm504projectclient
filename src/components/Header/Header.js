import React, { useEffect } from "react";
import { Button, Col, Row } from "react-bootstrap";
import "./Header.css";
import { setToken } from "../../redux/authSlice";
import { useDispatch } from "react-redux";
const Header = ({info}) => {
  const dispatch = useDispatch();
  const logoutOnClick = ({info}) => {
    dispatch(setToken(null));
    
  };
  useEffect(()=>{

  },[info])
  return (
    <div id="header-container">
      <Row>
        <Col>{info.companyName}</Col>
        <Col>
          <Button onClick={logoutOnClick}>Logout</Button>
        </Col>
      </Row>
    </div>
  );
};

export default Header;
