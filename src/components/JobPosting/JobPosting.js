import React from 'react'
import "./JobPosting.css"
import { Card,Button} from 'react-bootstrap'
const JobPosting = ({jobTitle,companyName,description, isApplicable,apply,review}) => {
  return (
    <Card style={{ width: '18rem' }}>
    <Card.Body>
      <Card.Title>{jobTitle}</Card.Title>
      <Card.Subtitle className="mb-2 text-muted">{companyName}</Card.Subtitle>
      <Card.Text>
        {description}
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      {isApplicable && <Button onClick={apply}> Apply</Button>}
      {!isApplicable && <Button onClick={review}>View Applicants</Button>}
    </Card.Footer>
  </Card>
  )
}

export default JobPosting