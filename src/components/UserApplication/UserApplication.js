import React, { useEffect, useState } from 'react'
import JobPosting from "../JobPosting/JobPosting";
import axios from "axios";
import { Container, Row, Col } from "react-bootstrap";
import "./userApplication.css"
const UserApplication = ({userId}) => {
    const [applications,setApplications] = useState([])
useEffect(()=>{
    getUserApplications(userId)
},[])
    const getUserApplications = (userId)=>{
        axios.get(`http://localhost:8080/jobapplication/byuser/${userId}`).then(res=>{
            const result = res.data;
            console.log(result)
            setApplications(result)
        })
    }
  return (
    <Container fluid>
      <Row>
        {applications.map((job, index) => (
          <Col xl={4} lg={4} key={index} className="myjob-col">
           <div className='application-info'>
               <div>{job.applicationInfo}</div>
               <div>Application Date: {job.date}</div>
           </div>
          </Col>
        ))}
      </Row>
    </Container>
  )
}

export default UserApplication