import React from "react";
import { useSelector } from "react-redux";
import { Row, Col, ListGroup } from "react-bootstrap";
import "./CompanyAbout.css"
const CompanyAbout = () => {
  const companyInfo = useSelector((state) => state.company.company);
  return (
    <div id="about-container">
    <Row>
      <Col>
        <h3>My Company Info</h3>
      </Col>
    </Row>
    <br/>
      <Row>
        <Col>
          <ListGroup>
            <ListGroup.Item>{companyInfo.companyName}</ListGroup.Item>
          </ListGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <ListGroup>
            <ListGroup.Item>{companyInfo.country}</ListGroup.Item>
          </ListGroup>
        </Col>
        <Col>
          <ListGroup>
            <ListGroup.Item>{companyInfo.city}</ListGroup.Item>
          </ListGroup>
        </Col>
      </Row>

      <Row>
        <Col>
          <ListGroup>
            <ListGroup.Item>{companyInfo.about}</ListGroup.Item>
          </ListGroup>
        </Col>
      </Row>
    </div>
  );
};

export default CompanyAbout;
