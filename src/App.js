import { useState } from "react";
import "./App.css";
import Home from "./pages/Home/Home";
import Login from "./pages/LoginPage/LoginPage";
import Signup from "./pages/SignupPage/SignupPage";
import SignUpMainPage from "./pages/SignupPage/SignUpMainPage";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { setToken } from "./redux/authSlice";
import jwt_decode from "jwt-decode";
import SignupPerson from "./pages/SignupPage/SignupPerson";
const App = () => {
  const token = useSelector((state) => state.token.token);
  const dispatch = useDispatch();
  const jwtToken = localStorage.getItem("jwtToken");


  if (jwtToken) {
    const decoded_jwtToken = jwt_decode(jwtToken);
    const currentTime = Date.now() / 1000;
    if (decoded_jwtToken.exp < currentTime) {
      dispatch(setToken(null));
      window.location.href = "/login";
    }
  }

  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route
            exact
            path="/"
            element={jwtToken ? <Home /> : <Navigate replace to="/login" />}
          />
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/signup" element={<SignUpMainPage />} />
          <Route exact path="/signupcomp" element={<Signup/>} />
          <Route exact path ="/signuppers" element={<SignupPerson/>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
