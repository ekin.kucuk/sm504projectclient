import { configureStore } from '@reduxjs/toolkit'
import authSlice from './authSlice'
import companySlice from './companySlice'
export const store = configureStore({
  reducer: {
      token: authSlice,
      company: companySlice,
  },
})