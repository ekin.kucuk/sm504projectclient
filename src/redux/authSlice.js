import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios';
const initialState = {
    token:null ,
  }

  export const authSlice = createSlice({
      name:"auth",
      initialState,
      reducers: {
          setToken: (state, action)=>{
              const data = action.payload
              if(data !==null){
                state.token = action.payload
                axios.defaults.headers.common["Authorization"] = data
              }else{
                delete axios.defaults.headers.common["Authorization"];
                state.token = null;
                localStorage.removeItem("jwtToken");
              }
              
          }
      }
  })

  export const {setToken} = authSlice.actions;

  export default authSlice.reducer;